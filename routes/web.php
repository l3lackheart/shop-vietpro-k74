<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Client Zone
 */
Route::group([
    'namespace' => 'Client'
], function () {
    
    // Client Home routes
    Route::group([], function() {
        Route::get('/', [
            'uses' => 'HomeController@index', // Home page
            'as' => 'client.home.index'
        ]);
        Route::get('gioi-thieu', [
            'uses' => 'HomeController@about', // About page
            'as' => 'client.home.about'
        ]);
        Route::get('lien-he', [
            'uses' => 'HomeController@contact', // Contact page
            'as' => 'client.home.contact'
        ]);
    });

    // Client Product routes
    Route::group([
        'prefix' => 'san-pham'
    ], function() {
        Route::get('/', [
            'uses' => 'ProductController@list', // List all products
            'as' => 'client.product.list'
        ]);
        Route::get('{slug}', [
            'uses' => 'ProductController@detail', // Product detail page
            'as' => 'client.product.detail'
        ]);
    });

    // Client Cart routes
    Route::group([
        'prefix' => 'gio-hang'
    ], function() {
        Route::get('/', [
            'uses' => 'CartController@index', // Get current cart
            'as' => 'client.cart.index'
        ]);
        Route::post('them/{product_id}', [
            'uses' => 'CartController@add', // Add new product to cart
            'as' => 'client.cart.add'
        ]);
        Route::post('sua/{row_id}/{quantity}', [ // Update row quantity
            'uses' => 'CartController@update',
            'as' => 'client.cart.update'
        ]);
        Route::delete('xoa/{row_id}', [
            'uses' => 'CartController@delete', // Delete an entry
            'as' => 'client.cart.delete'
        ]);
    });

    // Client Checkout routes
    Route::group([
        'prefix' => 'thanh-toan'
    ], function() {
        Route::get('/', [
            'uses' => 'CheckoutController@index', // Show checkout form
            'as' => 'client.checkout.index'
        ]);
        Route::post('/', [
            'uses' => 'CheckoutController@index', // Send checkout to server
            'as' => 'client.checkout.index'
        ]);
        Route::get('hoan-thanh', [
            'uses' => 'CheckoutController@complete', // Show complete page
            'as' => 'client.checkout.complete'
        ]);
    });
});

/**
 * Admin Zone
 */
Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin'
], function() {

    /**
     * Guest routes
     * Logged in administrator/user cannot access or send request via these routes
     */
    Route::group([
        'middleware' => 'guest'
    ], function() {
        Route::get('login',[
            'as' => 'admin.login.showLoginForm', // Show login form
            'uses' => 'LoginController@showLoginForm',
        ]);
        Route::post('login', [
            'as' => 'admin.login',
            'uses' => 'LoginController@login', // Attempt to login
        ]);
    });

    /**
     * Authenticated routes
     * Only logged in administrators can access or send request via these routes
     */
    Route::group([
        'middleware' => 'auth'
    ], function() {
        Route::post('logout',[
            'as' => 'admin.logout',
            'uses' => 'LoginController@logout' // Attempt to logout
        ]);

        // Admin dashboard routes
        Route::group([
            'prefix' => 'dashboard'
        ], function() {
            Route::get('/', [
                'as' => 'admin.dashboard.index',
                'uses' => 'DashboardController@index' // Admin dashboard page
            ]);
        });

        // Admin user routes
        Route::group([
            'prefix' => 'user'
        ], function() {
             Route::get('/', [
                'as' => 'client.user.index', // List user
                'uses' => 'UserController@index'
            ]);
            Route::get('detail', [
                'as' => 'client.user.detail', // Get user detail
                'uses' => 'UserController@show'
            ]);
            Route::post('detail', [
                'as' => 'client.user.detail', // Add new/update user
                'uses' => 'UserController@edit'
            ]);
            Route::post('delete', [
                'as' => 'client.user.delete', // Delete an user
                'uses' => 'UserController@destroy',
            ]);
        });

        // Admin Product Category routes
        Route::group([
            'prefix' => 'product-category'
        ], function() {
            Route::get('/', [
                'uses' => 'ProductCategoryController@index', // Get all product categories
                'as' => 'admin.product_category.index'
            ]);
            Route::get('detail', [
                'uses' => 'ProductCategoryController@show', // Get product category detail
                'as' => 'admin.product_category.detail'
            ]);
            Route::post('detail', [
                'uses' => 'ProductCategoryController@edit', // Add new/update product category
                'as' => 'admin.product_category.detail'
            ]);
            Route::post('delete', [
                'uses' => 'ProductCategoryController@destroy', // Delete a product category
                'as' => 'admin.product_category.delete'
            ]);
        });

        Route::get('/admin/product/detail', 'Admin\ProductController@detail');

        // Admin Product routes
        Route::group([
            'prefix' => 'product'
        ], function() {
            Route::get('/', [
                'uses' => 'ProductController@index', // Get all products
                'as' => 'admin.product.index'
            ]);
            Route::get('detail', [
                'uses' => 'ProductController@show', // Get product detail
                'as' => 'admin.product.detail'
            ]);
            Route::post('detail', [
                'uses' => 'ProductController@edit', // Add new/update product
                'as' => 'admin.product.detail'
            ]);
            Route::post('delete', [
                'uses' => 'ProductControler@destroy', // Delete a product
                'as' => 'admin.product.delete'
            ]);
        });

        // Admin Order routes
        Route::group([
            'prefix' => 'order'
        ], function() {
            Route::get('/', [
                'uses' => 'OrderController@index', // Get all orders
                'as' => 'admin.order.index'
            ]);
        });
    });
});