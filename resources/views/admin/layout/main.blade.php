<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') - Admin Store</title>
    <!-- css -->
    <link href="{{asset('manage/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('manage/css/datepicker3.css')}}" rel="stylesheet">
    <link href="{{asset('manage/css/styles.css')}}" rel="stylesheet">
    <!--Icons-->
    <script src="{{asset('manage/js/lumino.glyphs.js')}}"></script>
    <link rel="stylesheet" href="{{asset('Awesome/css/all.css')}}">
</head>

<body>
    <!-- header -->
    @include('admin.layout.header')
    <!-- header -->
    <!-- sidebar left-->
    @include('admin.layout.sidebar')
    <!--/. end sidebar left-->
    <!--main-->
    @yield('content')
    <!--end main-->
    <script src="{{asset('manage/js/jquery-1.11.1.min.js')}}"></script>
    <script src="{{asset('manage/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('manage/js/chart.min.js')}}"></script>
    <script src="{{asset('manage/js/chart-data.js')}}"></script>
    <script src="{{asset('manage/js/easypiechart.js')}}"></script>
    <script src="{{asset('manage/js/easypiechart-data.js')}}"></script>
    <script src="{{asset('manage/js/bootstrap-datepicker.js')}}"></script>

</body>

</html>