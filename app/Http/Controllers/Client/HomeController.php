<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Home page
     */
    public function index() {
        return view('client.index');
    }

    /**
     * About page
     */
    public function about() {
        return view('client.about');
    }

    /**
     * Contact page
     */
    public function contact() {
        return view('client.contact');
    }
}
