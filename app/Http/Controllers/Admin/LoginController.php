<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/admin/dashboard';

    /**
     * Show login form
     */
    public function showLoginForm(Request $request) {
        // return $request->name; //http://localhost:8000/admin/login?name=Bao
        return view('admin.auth.login');
    }

     /**
      * Attempt to login
      */
    // public function login(Request $request) {
    //     $request->validate([
    //         'email' => 'email',
    //         'password' => 'required' //password_confirmation
    //     ], [
    //         'email.email' => "Sai email rồi",
    //         'password.required' => 'Thiếu ô password rồi kìa'
    //     ]);

    //     if (auth()->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
    //         return redirect()->route('admin.dashboard.index');
    //     };


    //     session()->flash('message', 'Thông tin đăng nhập bạn cung cấp không tồn tại!');
    //     return redirect()->back()->withInput();
    // }

    /**
     * Attempt to logout
     */
    public function logout() {
        auth()->logout();
        return redirect()->route('admin.login.showLoginForm');
    }
}
