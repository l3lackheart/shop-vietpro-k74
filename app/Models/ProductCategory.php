<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'category_products';
    protected $guarded = ['id'];

    public function product()
    {
        return $this->belongsToMany('App\Models\Product', 'category_product_pivot', 'category_product_id', 'product_id');
    }

}
